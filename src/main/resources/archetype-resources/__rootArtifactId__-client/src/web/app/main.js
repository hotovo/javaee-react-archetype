var React = require('react');
var ReactDOM = require('react-dom');
var App = require('./App.jsx');
var ReactRouter = require('react-router');
var Router = ReactRouter.Router;
var IndexRoute = ReactRouter.IndexRoute;
var Route = ReactRouter.Route;

ReactDOM.render((
        <Router>
            <Route path="/" component={App}>
                <IndexRoute component={App}/>
            </Route>
        </Router>
    ), document.getElementById("root")
);
