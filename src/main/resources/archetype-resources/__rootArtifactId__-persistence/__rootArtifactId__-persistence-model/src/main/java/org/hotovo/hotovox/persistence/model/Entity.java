#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.persistence.model;

/**
 * @author Vladimir Hrusovsky
 */
public interface Entity {
	Long getId();
}
