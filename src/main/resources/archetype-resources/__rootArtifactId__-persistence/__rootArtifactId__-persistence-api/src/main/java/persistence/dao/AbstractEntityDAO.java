#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.persistence.dao;

import ${package}.persistence.model.Entity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * @author Vladimir Hrusovsky
 */
public abstract class AbstractEntityDAO<T extends Entity> implements EntityDAO<T> {
	private final Class<T> entityClass;

	@PersistenceContext
	private volatile EntityManager entityManager;

	protected AbstractEntityDAO(Class<T> entityClass) {
		this.entityClass = entityClass;
	}

	@Override
	public void save(T entity) {
		if (entity.getId() == null) {
			entityManager.persist(entity);
		} else {
			entityManager.merge(entity);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> getAll() {
		return (List<T>) entityManager.createQuery("FROM " + entityClass.getSimpleName());
	}

	@Override
	public T find(Long id) {
		return entityManager.find(entityClass, id);
	}

	@Override
	public void remove(T entity) {
		entityManager.remove(entity);
	}
}
