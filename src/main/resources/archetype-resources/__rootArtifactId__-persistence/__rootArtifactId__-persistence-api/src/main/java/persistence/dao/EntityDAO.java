#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.persistence.dao;

import ${package}.persistence.model.Entity;

import java.util.List;

/**
 * @author Vladimir Hrusovsky
 */
public interface EntityDAO<T extends Entity> {
	void save(T entity);

	List<T> getAll();

	T find(Long id);

	void remove(T entity);
}
